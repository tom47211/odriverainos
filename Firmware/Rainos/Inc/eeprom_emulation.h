/**
 ******************************************************************************
 * @file    EEPROM_Emulation/inc/eeprom.h
 * @author  MCD Application Team
 * @version V3.1.0
 * @date    07/27/2009
 * @brief   This file contains all the functions prototypes for the EEPROM
 *          emulation firmware library.
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EEPROM_H
#define __EEPROM_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

/* Exported constants --------------------------------------------------------*/
/* Define the STM32F10Xxx Flash page size depending on the used STM32 device */

/* EEPROM start address in Flash */

#define EEPROM_START_ADDRESS    FLASH_SECTOR_1

/* Pages 0 and 1 base and end addresses */

#define SECTOR0_BASE			0x08000000		//16K	/ Bootloader
#define SECTOR0_END				0x08003FFF		//		|

#define SECTOR1_BASE			0x08004000		//16K	/ EE Sector 0
#define SECTOR1_END				0x08007FFF		//		|

#define SECTOR2_BASE			0x08008000		//16K	/ EE Sector 1
#define SECTOR2_END				0x0800BFFF		//		|

#define SECTOR3_BASE			0x0800C000		//16K	/
#define SECTOR3_END				0x0800FFFF		//		|
#define SECTOR4_BASE			0x08010000		//64K	|
#define SECTOR4_END				0x0801FFFF		//		|
#define SECTOR5_BASE			0x08020000		//128K	|
#define SECTOR5_END				0x0803FFFF		//		|
#define SECTOR6_BASE			0x08040000		//128K	|
#define SECTOR6_END				0x0805FFFF		//		| Main Application
#define SECTOR7_BASE			0x08060000		//128K	|
#define SECTOR7_END				0x0807FFFF		//		|
#define SECTOR8_BASE			0x08080000		//128K	|
#define SECTOR8_END				0x0809FFFF		//		|
#define SECTOR9_BASE			0x080A0000		//128K	|
#define SECTOR9_END				0x080BFFFF		//		|

#define SECTOR10_BASE			0x080C0000		//128K	/ ODrive NVM Sector 0
#define SECTOR10_END			0x080DFFFF		//		|

#define SECTOR11_BASE			0x080E0000		//128K	/ ODrive NVM Sector 1
#define SECTOR11_END			0x080FFFFF		//		|






#define EE_SECTOR0_BASE			((uint32_t)FLASH_SECTOR_1)
#define EE_SECTOR0_END			((uint32_t)FLASH_SECTOR_2 - 1)

#define EE_SECTOR1_BASE			((uint32_t)FLASH_SECTOR_2)
#define EE_SECTOR1_END     		((uint32_t)FLASH_SECTOR_2 - 1)

/* Used Flash pages for EEPROM emulation */
#define SECTOR0                   ((uint16_t)0x0000)
#define SECTOR1                   ((uint16_t)0x0001)

/* No valid page define */
#define NO_VALID_SECTOR           ((uint16_t)0x00AB)

/* Sector status */
enum
{
	SECTOR_ERASED 	= 0xFFFF,
	RECEIVE_DATA 	= 0xEEEE,
	VALID_SECTOR 	= 0x0000,
	UNKNOWN			= 0xAAAA
}typedef sectorstatus;

enum
{
	EE_UNINITIALIZED,
	EE_INITIALIZED,
	EE_ERROR
}typedef ee_state;

struct sector
{
	uint8_t nbr;
	uint32_t base_addr;
	uint32_t end_addr;
	sectorstatus status;
}typedef sector;

struct eeprom_emulation
{
	ee_state state;

	sector sectors[2];
}typedef eeprom_emulation;



/* Valid pages in read and write defines */
#define READ_FROM_VALID_SECTOR    ((uint8_t)0x00)
#define WRITE_IN_VALID_SECTOR     ((uint8_t)0x01)

/* Page full define */
#define SECTOR_FULL               ((uint8_t)0x80)

/* Exported types ------------------------------------------------------------*/
enum
{
	FLASH_COMPLETE
}typedef FLASH_Status;
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
extern "C"
{
#endif

uint16_t EE_Init(void);
uint16_t EE_ReadVariable(uint16_t VirtAddress, uint16_t* Data);
uint16_t EE_WriteVariable(uint16_t VirtAddress, uint16_t Data);
uint16_t EE_Format();

#ifdef __cplusplus
}
#endif

#endif /* __EEPROM_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
