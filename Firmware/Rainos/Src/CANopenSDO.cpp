/*
 * CANopen.c
 *
 *  Created on: 07.06.2019
 *      Author: thomas
 */

#include "CANopenDictionary.hpp"
#include "CANopenSDO.hpp"
#include "can.h"
#include <string.h>
#include "cmsis_os.h"
#include <stdio.h>
#include "usart.h"

void SDO_Server::SDO_ServerThread()
{
	//print("start sdo server\n");

	uint32_t notificationValue;
	uint32_t timeoutValue;

	uint16_t index;
	uint8_t subindex;

	client_command_specifier ccs;	//client command specifier

	for (;;)
	{
		timeoutValue = ulTaskNotifyTake(pdTRUE, 1000);

		if (state == Waiting)
		{
			if (timeoutValue == 1)
			{

				ccs = (client_command_specifier) (((CO_data[0]) >> 5) & 0b111);

				if (ccs == INITIATE_DOWNLOAD_REQUEST)
				{
					SDO_InitiateDownloadRequestReceived();
				}
				else if (ccs == INITIATE_UPLOAD_REQUEST)
				{
					SDO_InitiateUploadRequestReceived();

				}
				else if (ccs == CCS_ABORT)
				{
					state = Waiting;
				}
				else
				{
					SDO_AbortTransfer(SDO_ERR_COMMAND_SPECIFIER);
				}

			}
			else
			{
				// do nothing
			}

		}
		else if (state == UploadInProgress)
		{

			if (timeoutValue == 1)
			{
				ccs = (client_command_specifier) ((CO_data[0] >> 5) & 0b111);

				if (ccs == UPLOAD_SEGMENT_REQUEST)
				{
					SDO_UploadSegmentResponse();
				}
				else if (ccs == CCS_ABORT)
				{
					state = Waiting;
				}
				else
				{
					SDO_AbortTransfer(SDO_ERR_COMMAND_SPECIFIER);
				}
			}
			else
			{
				SDO_AbortTransfer(SDO_ERR_TIMEOUT);
			}
		}
		else if (state == DownloadInProgress)
		{

			if (timeoutValue == 1)
			{
				ccs = (client_command_specifier) ((CO_data[0] >> 5) & 0b11);

				if (ccs == DOWNLOAD_SEGMENT_REQUEST)
				{
					SDO_DownloadSegment();
				}
				else if (ccs == CCS_ABORT)
				{
					state = Waiting;
				}
				else
				{
					SDO_AbortTransfer(SDO_ERR_COMMAND_SPECIFIER);
				}
			}
			else
			{
				SDO_AbortTransfer(SDO_ERR_TIMEOUT);
			}
		}

	}

}
void start_sdo_server_wrapper(void *arg)
{
	reinterpret_cast<SDO_Server*>(arg)->SDO_ServerThread();
}

void SDO_Server::start_thread()
{
	osThreadDef(sdo_server_def, (os_pthread ) start_sdo_server_wrapper, thread_priority, 0, 1000); //TODO Stacksize
	thread_id = osThreadCreate(osThread(sdo_server_def), this);

}

void SDO_Server::SDO_InitiateDownloadRequestReceived()
{
	state = DownloadInProgress;

	uint8_t e = (CO_data[0] >> 1) & 1;
	CO_index = CO_data[1] | CO_data[2] << 8;
	CO_subindex = CO_data[3];

	if (e == 1)
	{
		SDO_DownloadExpedited();
	}
	else
	{
		SDO_Download();
	}

}

void SDO_Server::SDO_DownloadExpedited()
{

	int8_t res = getCurrentObject();
	if (res == 1)
	{
		SDO_AbortTransfer(SDO_ERR_INDEX);
		return;
	}

	if (res == 2)
	{
		SDO_AbortTransfer(SDO_ERR_SUBINDEX);
		return;
	}

	if (dictionary->entries[current_object].accessType == read_only || dictionary->entries[current_object].accessType == const_param)
	{
		SDO_AbortTransfer(SDO_ERR_WRITE_PROTECTED);
		return;
	}

	uint8_t s = CO_data[0] >> 0 & 1;
	uint8_t n = (CO_data[0] >> 2) & 0b11;
	uint8_t size;

	if (dictionary->entries[current_object].datatype != none)
	{
		if (s == 1)
		{
			size = 4 - n;
			if (size > dictionary->entries[current_object].size)
			{
				SDO_AbortTransfer(SDO_ERR_DATA_TYPE_LENGTH);
				return;
			}
		}
		else
		{
			size = dictionary->entries[current_object].size;
		}

		CO_value = 0;

		if (dictionary->entries[current_object].datatype == string)
		{
			strcpy((char*) dictionary->entries[current_object].value, (const char*) CO_data + 4);
		}
		else if (dictionary->entries[current_object].datatype == datablock)
		{
			memcpy(dictionary->entries[current_object].value, CO_data + 4, size);
		}
		else if (dictionary->entries[current_object].datatype != none)
		{
			//Value check for all number types
			if (dictionary->entries[current_object].min_value < dictionary->entries[current_object].max_value)
			{
				switch (dictionary->entries[current_object].datatype)
				{
					case boolean:
						CO_value = (bool) CO_data[4];
						break;
					case u_int_8:
						CO_value = (uint8_t) CO_data[4];
						break;

					case int_8:
						CO_value = (int8_t) CO_data[4];
						break;

					case u_int_16:
						memcpy(&CO_value, CO_data + 4, 2);
						CO_value = (uint16_t) CO_value;
						break;

					case int_16:
						memcpy(&CO_value, CO_data + 4, 2);
						CO_value = (int16_t) CO_value;
						break;

					case u_int_32:
						memcpy(&CO_value, CO_data + 4, 4);
						CO_value = (uint32_t) CO_value;
						break;

					case int_32:
						memcpy(&CO_value, CO_data + 4, 4);
						CO_value = (int32_t) CO_value;
						break;

					case float_factor_10:
					case float_factor_100:
					case float_factor_1000:
						if (dictionary->entries[current_object].size == 2)
						{
							memcpy(&CO_value, CO_data + 4, 2);
							CO_value = (int16_t) CO_value;
						}
						else if (dictionary->entries[current_object].size == 4)
						{
							memcpy(&CO_value, CO_data + 4, 4);
							CO_value = (int32_t) CO_value;
						}
						break;
					default:
						break;
				}

				if (CO_value > dictionary->entries[current_object].max_value)
				{
					SDO_AbortTransfer(SDO_ERR_VALUE_TOO_HIGH);
					return;
				}
				if (CO_value < dictionary->entries[current_object].min_value)
				{
					SDO_AbortTransfer(SDO_ERR_VALUE_TOO_LOW);
					return;
				}
			}

			//Copy values
			switch (dictionary->entries[current_object].datatype)
			{
				case float_factor_10:
				case float_factor_100:
				case float_factor_1000:
					memcpy(&CO_value, CO_data + 4, dictionary->entries[current_object].size);
					if (dictionary->entries[current_object].size == 2)
					{
						CO_value = (int16_t) CO_value;
					}
					else if (dictionary->entries[current_object].size == 4)
					{
						CO_value = (int32_t) CO_value;
					}

					if (dictionary->entries[current_object].datatype == float_factor_10)
					{
						*(float*) dictionary->entries[current_object].value = (float) CO_value / 10.0f;
					}
					else if (dictionary->entries[current_object].datatype == float_factor_100)
					{
						*(float*) dictionary->entries[current_object].value = (float) CO_value / 100.0f;
					}
					else if (dictionary->entries[current_object].datatype == float_factor_1000)
					{
						*(float*) dictionary->entries[current_object].value = (float) CO_value / 1000.0f;
					}

					break;

				case float_ieee:
					memcpy(dictionary->entries[current_object].value, CO_data + 4, 4);
					break;


				default:
					memcpy(dictionary->entries[current_object].value, CO_data + 4, dictionary->entries[current_object].size);

			}
		}
	}

	uint8_t return_value = 0;

	//When changing node id the response is sent with the new id

	if (dictionary->entries[current_object].function != NULL)
	{
		return_value = 1;
		void *arg_ptr;
		int (*func_ptr)(void*);

		func_ptr = (int (*)(void*)) dictionary->entries[current_object].function;
		arg_ptr = dictionary->entries[current_object].argument;
		return_value = func_ptr(arg_ptr);
	}
	if(return_value == 0)
	{
		SDO_InitiateDownloadResponse();
		state = Waiting;

	}
	else if(return_value == 2)
	{
		SDO_AbortTransfer(SDO_ERR_TRANSFER_DEVICE_STATE);
	}
	else
	{
		SDO_AbortTransfer(SDO_ERR_TRANSFER_GENERAL);
	}


}

void SDO_Server::SDO_Download()
{

	int8_t res = getCurrentObject();

	if (res == 1)
	{
		SDO_AbortTransfer(SDO_ERR_INDEX);
		return;
	}

	if (res == 2)
	{
		SDO_AbortTransfer(SDO_ERR_SUBINDEX);
		return;
	}

	if (dictionary->entries[current_object].accessType == read_only || dictionary->entries[current_object].accessType == const_param)
	{
		SDO_AbortTransfer(SDO_ERR_WRITE_PROTECTED);
		return;
	}

	CO_size = CO_data[4] | CO_data[5] << 8 | CO_data[6] << 16 | CO_data[7] << 24;

	if (CO_size > dictionary->entries[current_object].size)
	{
		SDO_AbortTransfer(SDO_ERR_DATA_TYPE_LENGTH);
		return;
	}

	SDO_InitiateDownloadResponse();

	CO_rwIndex = 0;
	CO_toggle = 1;

}

void SDO_Server::SDO_InitiateDownloadResponse()
{
	uint32_t txMailbox;

	tx_header.StdId = SDO_SERVER + 3;//TODO node_id;

	tx_data[0] = INITIATE_DOWNLOAD_RESPONSE << 5;
	tx_data[1] = CO_index;
	tx_data[2] = CO_index >> 8;
	tx_data[3] = CO_subindex;
	tx_data[4] = 0;
	tx_data[5] = 0;
	tx_data[6] = 0;
	tx_data[7] = 0;


//	xSemaphoreTake(CanTxMutexHandle, 100);
	HAL_CAN_AddTxMessage(canHandle, &tx_header, tx_data, &txMailbox);
//	xSemaphoreGive(CanTxMutexHandle);


}

void SDO_Server::SDO_DownloadSegment()
{
	if (CO_toggle == (CO_data[0] & (1 << 4)))
	{
		SDO_AbortTransfer(SDO_ERR_TOGGLE_BIT);
		return;
	}

	CO_toggle = (CO_data[0] >> 4) & 1;
	uint8_t n = (CO_data[0] >> 1) & 0b111;
	uint8_t c = CO_data[0] & 1;  // if c == 0 there are still segments left
								 //    c == 1 this segment is the last
	uint8_t bytes = 7 - n;

	memcpy(CO_buffer + CO_rwIndex, CO_data + 1, bytes);

	CO_rwIndex += bytes;

	if (c == 1)
	{
		if (dictionary->entries[current_object].datatype == string)
		{
			strcpy((char*) dictionary->entries[current_object].value, (const char*) (CO_buffer));
		}
		else
		{
			memcpy(dictionary->entries[current_object].value, CO_buffer, CO_rwIndex);
		}

		memset(dictionary->entries[current_object].value + CO_rwIndex, 0, dictionary->entries[current_object].size - CO_rwIndex);

		int return_value = 0;

		if (dictionary->entries[current_object].function != NULL)
		{
			return_value = 1;
			void *arg_ptr;
			int (*func_ptr)(void*);

			func_ptr = (int (*)(void*)) dictionary->entries[current_object].function;
			arg_ptr = dictionary->entries[current_object].argument;

			return_value = func_ptr(arg_ptr);
		}

		//TODO canopen function return values are mehhhh
		if (return_value == 0)
		{
			SDO_DownloadSegmentResponse();
			state = Waiting;


		}
		else if (return_value == 2)
		{
			SDO_AbortTransfer(SDO_ERR_TRANSFER_DEVICE_STATE);

		}
		else
		{
			SDO_AbortTransfer(SDO_ERR_TRANSFER_GENERAL);
		}


	}
	else
	{
		SDO_DownloadSegmentResponse();
	}

}

void SDO_Server::SDO_DownloadSegmentResponse()
{
	uint32_t txMailbox;

	tx_header.StdId = SDO_SERVER + 3;//TODO node_id;

	tx_data[0] = DOWNLOAD_SEGMENT_RESPONSE << 5;
	tx_data[0] |= ((CO_toggle & 1) << 4);
	tx_data[1] = 0;
	tx_data[2] = 0;
	tx_data[3] = 0;
	tx_data[4] = 0;
	tx_data[5] = 0;
	tx_data[6] = 0;
	tx_data[7] = 0;

//	xSemaphoreTake(CanTxMutexHandle, 100);
	HAL_CAN_AddTxMessage(canHandle, &tx_header, tx_data, &txMailbox);
//	xSemaphoreGive(CanTxMutexHandle);

}

void SDO_Server::SDO_InitiateUploadRequestReceived()
{
	state = UploadInProgress;

	CO_index = CO_data[1] | CO_data[2] << 8;
	CO_subindex = CO_data[3];
	uint8_t e = (CO_data[0] >> 1) & 1;

	uint8_t res = getCurrentObject();

	if (res == 1)
	{
		SDO_AbortTransfer(SDO_ERR_INDEX);
		return;
	}
	if (res == 2)
	{
		SDO_AbortTransfer(SDO_ERR_SUBINDEX);
		return;
	}
	if (dictionary->entries[current_object].accessType == write_only)
	{
		SDO_AbortTransfer(SDO_ERR_READ_PROTECTED);
		return;
	}
	if (dictionary->entries[current_object].size <= 4)
	{
		SDO_UploadResponseExpedited();
	}
	else
	{
		SDO_Upload();
	}
}

void SDO_Server::SDO_UploadResponseExpedited()
{
	uint8_t e = 1;
	uint8_t s = 1;
	uint8_t n = 4 - dictionary->entries[current_object].size;

	uint32_t txMailbox;

	tx_header.StdId = SDO_SERVER + 3;//TODO node_id;

	memset(tx_data, 0, 8);

	tx_data[0] = INITIATE_UPLOAD_RESPONSE << 5;
	tx_data[0] |= s;
	tx_data[0] |= e << 1;
	tx_data[0] |= (n & 0b11) << 2;
	tx_data[1] = CO_index;
	tx_data[2] = CO_index >> 8;
	tx_data[3] = CO_subindex;

	switch (dictionary->entries[current_object].datatype)
	{
		//TODO: check nullpointer check so program doesnt crash when dereferencing

		case float_factor_10:
			CO_value = (uint32_t)( (*(float*) dictionary->entries[current_object].value) * 10);
			memcpy(tx_data + 4, &CO_value, dictionary->entries[current_object].size);
			break;

		case float_factor_100:
			CO_value = (uint32_t)( (*(float*) dictionary->entries[current_object].value) * 100);
			memcpy(tx_data + 4, &CO_value, dictionary->entries[current_object].size);
			break;

		case float_factor_1000:
			CO_value = (uint32_t)( (*((float*)dictionary->entries[current_object].value)) * 1000);
			memcpy(tx_data + 4, &CO_value, dictionary->entries[current_object].size);
			break;

		case string:
			strcpy((char*) tx_data + 4, (const char*) dictionary->entries[current_object].value);
			break;
		case boolean:
		case u_int_8:
		case int_8:
		case u_int_16:
		case int_16:
		case u_int_32:
		case int_32:
		case float_ieee:
			memcpy(tx_data + 4, dictionary->entries[current_object].value, dictionary->entries[current_object].size);
			break;
		case u_int_64:
		case int_64:
		default:
			SDO_AbortTransfer(SDO_ERR_DATA_TYPE_LENGTH);
			return;

	}
//	xSemaphoreTake(CanTxMutexHandle, 100);
	HAL_CAN_AddTxMessage(canHandle, &tx_header, tx_data, &txMailbox);
//	xSemaphoreGive(CanTxMutexHandle);



	state = Waiting;
}

void SDO_Server::SDO_Upload()
{
	CO_rwIndex = 0;
	CO_toggle = 1;
	if (dictionary->entries[current_object].datatype == string)
	{
		CO_size = strlen((const char*) dictionary->entries[current_object].value);
	}
	else
	{
		CO_size = dictionary->entries[current_object].size;
	}
	SDO_UploadResponse();
}

void SDO_Server::SDO_UploadResponse()
{
	uint8_t e = 0;
	uint8_t s = 1;
	uint8_t n = 0;

	uint32_t txMailbox;

	tx_header.StdId = SDO_SERVER + 3;//TODO node_id;

	tx_data[0] = INITIATE_UPLOAD_RESPONSE << 5;
	tx_data[0] |= s;
	tx_data[0] |= e << 1;
	tx_data[0] |= (n & 0b11) << 2;

	tx_data[1] = CO_index;
	tx_data[2] = CO_index >> 8;
	tx_data[3] = CO_subindex;

	tx_data[4] = CO_size;
	tx_data[5] = CO_size >> 8;
	tx_data[6] = 0;  //because size is only 16 bit wide
	tx_data[7] = 0;


//	xSemaphoreTake(CanTxMutexHandle, 100);
	HAL_CAN_AddTxMessage(canHandle, &tx_header, tx_data, &txMailbox);
//	xSemaphoreGive(CanTxMutexHandle);

}

void SDO_Server::SDO_UploadSegmentResponse()
{

	if (CO_toggle == ((CO_data[0] >> 4) & 1))
	{
		SDO_AbortTransfer(SDO_ERR_TOGGLE_BIT);
		return;
	}

	CO_toggle = (CO_data[0] >> 4) & 1;

	uint16_t bytes_left = CO_size - CO_rwIndex;

	uint8_t n;
	uint8_t c;

	if (bytes_left >= 7)
	{
		n = 0;
		c = 0;
	}
	else
	{
		n = 7 - bytes_left;
		c = 1;
	}

//	print("bytes left: %d\n",bytes_left);
//	print("n: c: %d %d\n",n, c);
//	print("rw index: %d\n",CO_rwIndex);

	tx_header.StdId = SDO_SERVER + 3;//TODO node_id;

	uint32_t txMailbox;

	tx_data[0] = c;
	tx_data[0] |= (n & 0b111) << 1;
	tx_data[0] |= CO_toggle << 4;
	tx_data[0] |= UPLOAD_SEGMENT_RESPONSE << 5;

	memcpy(tx_data + 1, (uint8_t*) dictionary->entries[current_object].value + CO_rwIndex, 7 - n);
	CO_rwIndex += 7;


//	xSemaphoreTake(CanTxMutexHandle, 100);
	HAL_CAN_AddTxMessage(canHandle, &tx_header, tx_data, &txMailbox);
//	xSemaphoreGive(CanTxMutexHandle);


	if (bytes_left < 7)
	{
		state = Waiting;
	}

}

void SDO_Server::SDO_AbortTransfer(uint32_t abortCode)
{
	state = Waiting;

	uint32_t txMailbox;

	tx_header.StdId = SDO_SERVER + 3;//TODO node_id;

	tx_data[0] = (CCS_ABORT << 5);
	tx_data[1] = 0;
	tx_data[2] = 0;
	tx_data[3] = 0;
	tx_data[4] = abortCode;
	tx_data[5] = abortCode >> 8;
	tx_data[6] = abortCode >> 16;
	tx_data[7] = abortCode >> 24;


//	xSemaphoreTake(CanTxMutexHandle, 100);
	HAL_CAN_AddTxMessage(canHandle, &tx_header, tx_data, &txMailbox);
//	xSemaphoreGive(CanTxMutexHandle);

}

int8_t SDO_Server::getCurrentObject()
{
	uint8_t index_found = 0;
	uint8_t subindex_found = 0;

	for (int i = 0; i < dictionary->getCurrentSize(); i++)
	{
		if (dictionary->entries[i].index == 0)
		{
			break;
		}

		if (CO_index == dictionary->entries[i].index)
		{
			index_found = 1;

			if (CO_subindex == dictionary->entries[i].subindex)
			{
				subindex_found = 1;
				current_object = i;
				break;
			}
		}
	}

	if (index_found == 0)
	{
		return 1;
	}
	else if (subindex_found == 0)
	{
		return 2;
	}
	return 0;
}

void SDO_Server::msgReceived(uint8_t data[8])
{
	memcpy(CO_data, data, 8);
	xTaskNotifyGive(thread_id);
}

