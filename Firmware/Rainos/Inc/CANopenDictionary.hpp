/*
 * CANopenDictionary.h
 *
 *  Created on: 07.08.2019
 *      Author: thomas
 */

#ifndef CANOPENDICTIONARY_HPP_
#define CANOPENDICTIONARY_HPP_

#include "CANopenSDO.hpp"

#define DICTIONARY_MAX_SIZE 400




struct
{
	void *value;
	datatype_t datatype;	//This represents the data type of the actual value in RAM
	uint16_t size;	//This is the size that is used for can transfer and EEPROM storage

	/*
	 * pointer to function that can be called by write access of the dictionary entry
	 * these functions shall return true if it was successful, or false
	 * the sdo server will then send the response accordingly
	 */
	uint8_t (*function)();
	void *argument;		//argument ptr that can be passed to this function
	uint16_t index;
	uint8_t subindex;
	accessType_t accessType;
	int64_t min_value;	//value check is only possible for number type objects
	int64_t max_value;

}typedef CANopenDictionaryEntry_t;

class CANopenDictionary
{
private:
	uint32_t current_size;


public:

	/*
	 * Only a pointer to the actual dictionary entries is stored.
	 * This makes it easier to use different allocations
	 * (for example if you want to create entries dynamically)
	 */

	CANopenDictionaryEntry_t *entries;
	const uint32_t max_size;

	//Constructor
	CANopenDictionary(CANopenDictionaryEntry_t *_entries, uint32_t _max_size);

	void addEntry(uint16_t _index, uint8_t _subindex, datatype_t _datatype, void *_value,  accessType_t _accesType, uint32_t _size, uint8_t (*_function)(),
			void *_argument, int64_t _min_value,	int64_t _max_value);
	void addFunction(uint16_t _index, uint8_t _subindex, uint8_t (*function)(), void *_argument);

	void restoreDefaults();
	void saveParameter();
	void loadParameter();

	uint32_t getCurrentSize()
	{
		return current_size;
	}

private:
	int32_t getArrayIndex(uint16_t index, uint8_t subindex);

};

extern CANopenDictionary dictionary;

#define SDO_CLIENT 0x600
#define SDO_SERVER 0x580

struct
{
	uint32_t error_register;
	uint32_t manufacturer_status_register;

	char manufacturer_device_name[30] = "Heros Drive Firmware";
	char manufacturer_hardware_version[30] = "1.2.3.4.5.6.7.8";
	char manufacturer_software_version[30] = "IR_ROCTR1 REV_B";

	uint32_t restoreDefaultParameter_password = 0;
	uint32_t storeParameter_password = 0;
	uint32_t writeProtection_password = 0;

	char vehicle_serial_number[30] = "SP-223ZR-1905";
	char pcb_serial_number[9] = "asdfasdf";

	uint64_t commissioning_date_unix = 1572359581;

	uint8_t dfu_initiate_password = 0;

	uint32_t program_crc32 = 0;
	uint32_t program_length = 0;

	uint8_t node_id;

}typedef volatile_SDO_t;


void init_dictionary(CANopenDictionary *);

extern volatile_SDO_t volatile_SDO;

#define IS_MASTER 1


#endif /* CANOPENDICTIONARY_HPP_ */
