/*
 * CANopenSDO.h
 *
 *  Created on: 07.06.2019
 *      Author: thomas
 */

#ifndef CANOPENSDO_HPP_
#define CANOPENSDO_HPP_

#include "can.h"
#include "cmsis_os.h"

/*
 * SDO Abort Codes
 */

#define SDO_ERR_TOGGLE_BIT						0x05030000	// Toggle bit not alternated
#define SDO_ERR_TIMEOUT							0x05040000	// SDO Protocol timed out
#define SDO_ERR_COMMAND_SPECIFIER				0x05040001 	// Client/server command specifier not valid or unknown.
#define SDO_ERR_DATA_TYPE_LENGTH				0x06070010 	// Data type does not match, length of service parameter does not match

#define SDO_ERR_UNSUPPORTED_ACCESS				0x06010000 	// Unsupported access to an object.
#define SDO_ERR_READ_PROTECTED					0x06010001 	// Attempt to read a write only object.
#define SDO_ERR_WRITE_PROTECTED					0x06010002 	// Attempt to write a read only object.

#define SDO_ERR_INDEX							0x06020000 	// Object does not exist in the object dictionary.

#define SDO_ERR_SUBINDEX						0x06090011 	// Sub-index does not exist.

#define SDO_ERR_INVALID_VALUE					0x06090030 	// Invalid value for parameter (download only).
#define SDO_ERR_VALUE_TOO_HIGH					0x06090031 	// Value of parameter written too high (download only).
#define SDO_ERR_VALUE_TOO_LOW					0x06090032 	// Value of parameter written too low (download only).
#define SDO_ERR_GENERAL_ERROR					0x08000000
#define SDO_ERR_TRANSFER_GENERAL				0x08000020	// Data cannot be transferred or stored to the application.
#define SDO_ERR_TRANSFER_DEVICE_STATE			0x08000022


enum
{
	read_write = 0,
	read_only = 1,
	write_only = 2,
	const_param = 3
}typedef accessType_t;

enum
{
	none,
	boolean,
	u_int_8,			// Integer types
	int_8,
	u_int_16,
	int_16,
	u_int_32,
	int_32,
	u_int_64,
	int_64,
	float_ieee,
	double_ieee,
	float_factor_1000,	// Values are currently transferred as 2 byte integers, stored in eep as 2 byte int and in RAM as float
	float_factor_100,
	float_factor_10,
	string,				// String that is terminated with \0 character. Must be smaller than "size" parameter
	datablock			// Block of data, exactly the size of "size" parameter. Can be used to transmit various data structures

}typedef datatype_t;

class CANopenDictionary;

class SDO_Server
{
#define MAX_SDO_LENGTH 500
private:
	void SDO_InitiateDownloadRequestReceived();
	void SDO_DownloadExpedited();
	void SDO_InitiateDownloadResponse();
	void SDO_Download();
	void SDO_DownloadSegment();
	void SDO_DownloadSegmentResponse();

	void SDO_AbortTransfer(uint32_t abortCode);

	int8_t getCurrentObject();

	/*
	 * SDO_Upload: Client reads data from the server (the microcontroller)
	 */
	void SDO_InitiateUploadRequestReceived();
	void SDO_UploadResponseExpedited();
	void SDO_Upload();
	void SDO_UploadResponse();
	void SDO_UploadSegmentResponse();

	bool CO_toggle;
	uint8_t CO_data[8];
	uint8_t CO_buffer[MAX_SDO_LENGTH];
	int32_t CO_value;
	uint32_t CO_size;
	uint16_t CO_rwIndex;
	int16_t current_object;
	uint16_t CO_index;
	uint8_t CO_subindex;

	CAN_HandleTypeDef *canHandle = &hcan1;

	CAN_TxHeaderTypeDef tx_header =
	{
		.StdId = 0,
		.ExtId = 0,
		.IDE = CAN_ID_STD,
		.RTR = CAN_RTR_DATA,
		.DLC = 8,
		.TransmitGlobalTime = DISABLE
	};

	uint8_t tx_data[8];

	CANopenDictionary *dictionary;

public:
	enum
	{
		Waiting, UploadInProgress, DownloadInProgress

	}typedef sdo_state_t;

	enum
	{
		DOWNLOAD_SEGMENT_REQUEST = 0,
		INITIATE_DOWNLOAD_REQUEST = 1,
		INITIATE_UPLOAD_REQUEST = 2,
		UPLOAD_SEGMENT_REQUEST = 3,
		CCS_ABORT = 4
	}typedef client_command_specifier;

	enum
	{
		UPLOAD_SEGMENT_RESPONSE = 0,
		DOWNLOAD_SEGMENT_RESPONSE = 1,
		INITIATE_DOWNLOAD_RESPONSE = 3,
		INITIATE_UPLOAD_RESPONSE = 2,
		SCS_ABORT = 4
	}typedef server_command_specifier;


	SDO_Server(CAN_HandleTypeDef *hcan, CANopenDictionary *dictionary_) : canHandle(hcan), dictionary(dictionary_)
	{
	}

	sdo_state_t state;

	osPriority thread_priority = osPriorityNormal;
	osThreadId thread_id;

	void start_thread();
	void SDO_ServerThread();
	void msgReceived(uint8_t data[8]);
};

#endif /* CANOPENSDO_HPP_ */
