/*
 * can_decode.c
 *
 *  Created on: 21.05.2019
 *      Author: thomas
 */
#include <CANopenDictionary.hpp>
#include "innok_can_interface.hpp"

#include "can.h"
#include "FreeRTOS.h"
#include "usart.h"
#include "cmsis_os.h"
#include "pump.hpp"
#include "odrive_main.h"
#include "cmsis_os.h"
#include "CANopenSDO.hpp"

extern SDO_Server sdo_server;
extern CAN_Interface can_interface;
extern Pump *pumps[2];

void CAN_Interface::set_filter()
{
	CAN_FilterTypeDef filterConfig =
	{
		.FilterIdHigh = 0x601<<5,
		.FilterIdLow = 0<<5,
		.FilterMaskIdHigh = 0x7FF,
		.FilterMaskIdLow = 0<<5,
		.FilterFIFOAssignment = CAN_FILTER_FIFO0,
		.FilterBank = 0,
		.FilterMode = CAN_FILTERMODE_IDMASK,
		.FilterScale = CAN_FILTERSCALE_16BIT,
		.FilterActivation = ENABLE,
		.SlaveStartFilterBank = 10
	};

	HAL_CAN_ConfigFilter(this->canHandle, &filterConfig);
}
void CAN_Interface::server_thread()
{
	uint32_t notificationValue;

	set_filter();
	HAL_CAN_ActivateNotification(canHandle, CAN_IT_RX_FIFO0_MSG_PENDING);

	HAL_CAN_Start(&hcan1);
//	uart_print("ver started\n");


	for (;;)
	{

		notificationValue = ulTaskNotifyTake( pdTRUE, 500); 			// Block 500 ms

		if (notificationValue == pdTRUE)
		{

			// Analog stick values of remote control
			if (rx_header.StdId == 0x300)
			{
				pumps[0]->cmdReceived(rx_data[0], rx_data[1]);
			}

			// Buttons and switches of remote control
			else if (rx_header.StdId == 0x301)
			{
				pumps[0]->cmdReceived(rx_data[0], rx_data[1]);
			}

			// CANopen SDO messages
			else if (rx_header.StdId == 0x600u + 3)//TODO node_id)
			{
				sdo_server.msgReceived(rx_data);
			}

		}

		else
		{
			//No can message Received for 500 ms

			//TODO error handling
		}

	}
}

void CAN_Interface::transmitter_thread()
{
	//print("Start can send task\n");
	uint32_t txMailbox;

	//Messages that only need to be sent once at startup go here

	tx_header.StdId = 0x0;
	tx_header.DLC = 2;
	tx_header.IDE = CAN_ID_STD;
	tx_data[0] = 0x01;
	tx_data[1] = 0x00;

	//	xSemaphoreTake(CanTxMutexHandle, 200);
	HAL_CAN_AddTxMessage(&hcan1, &tx_header, tx_data, &txMailbox);
	//	xSemaphoreGive(CanTxMutexHandle);
	uint32_t tick_delay_1000 = 0;
	uint32_t tick_delay_250 = 0;
	uint32_t tick_delay_100 = 0;

	uint32_t currentTick;
	uint8_t x = 0;

	for (;;)
	{
		currentTick = osKernelSysTick();




		if(currentTick - tick_delay_250 > 250)
		{

			tick_delay_250 = currentTick;


			uint16_t targetVelInt;

			tx_header.StdId = 0xDF;
			tx_header.DLC = 8;
			tx_data[0] = pumps[0]->state;
			tx_data[1] = pumps[0]->iqTripCounter;
			targetVelInt =  (int16_t) pumps[0]->targetVel;
			tx_data[2] = targetVelInt;
			tx_data[3] = targetVelInt >> 8;

			tx_data[4] = pumps[1]->state;
			tx_data[5] = pumps[1]->iqTripCounter;
			targetVelInt = (int16_t) pumps[1]->targetVel;
			tx_data[6] = targetVelInt;
			tx_data[7] = targetVelInt >> 8;


			canSend();

			/* ODrive Error Messages */

			tx_header.StdId = 0xE0;
			tx_header.DLC = 8;
			tx_data[0] = axes[0]->error_;
			tx_data[1] = axes[0]->error_ >> 8;
			tx_data[2] = axes[0]->error_ >> 16;
			tx_data[3] = axes[0]->error_ >> 24;
			tx_data[4] = axes[1]->error_;
			tx_data[5] = axes[1]->error_ >> 8;
			tx_data[6] = axes[1]->error_ >> 16;
			tx_data[7] = axes[1]->error_ >> 24;

			canSend();

			tx_header.StdId = 0xE1;
			tx_header.DLC = 8;
			tx_data[0] = axes[0]->motor_.error_;
			tx_data[1] = axes[0]->motor_.error_ >> 8;
			tx_data[2] = axes[0]->motor_.error_ >> 16;
			tx_data[3] = axes[0]->motor_.error_ >> 24;
			tx_data[4] = axes[1]->motor_.error_;
			tx_data[5] = axes[1]->motor_.error_ >> 8;
			tx_data[6] = axes[1]->motor_.error_ >> 16;
			tx_data[7] = axes[1]->motor_.error_ >> 24;

			canSend();

			tx_header.StdId = 0xE2;
			tx_header.DLC = 8;
			tx_data[0] = axes[0]->motor_.error_;
			tx_data[1] = axes[0]->motor_.error_ >> 8;
			tx_data[2] = axes[0]->motor_.error_ >> 16;
			tx_data[3] = axes[0]->motor_.error_ >> 24;
			tx_data[4] = axes[1]->motor_.error_;
			tx_data[5] = axes[1]->motor_.error_ >> 8;
			tx_data[6] = axes[1]->motor_.error_ >> 16;
			tx_data[7] = axes[1]->motor_.error_ >> 24;

			canSend();

			tx_header.StdId = 0xE3;
			tx_header.DLC = 8;
			tx_data[0] = axes[0]->encoder_.error_;
			tx_data[1] = axes[0]->encoder_.error_ >> 8;
			tx_data[2] = axes[0]->encoder_.error_ >> 16;
			tx_data[3] = axes[0]->encoder_.error_ >> 24;
			tx_data[4] = axes[1]->encoder_.error_;
			tx_data[5] = axes[1]->encoder_.error_ >> 8;
			tx_data[6] = axes[1]->encoder_.error_ >> 16;
			tx_data[7] = axes[1]->encoder_.error_ >> 24;

			canSend();

			tx_header.StdId = 0xE4;
			tx_header.DLC = 8;
			tx_data[0] = axes[0]->sensorless_estimator_.error_;
			tx_data[1] = axes[0]->sensorless_estimator_.error_ >> 8;
			tx_data[2] = axes[0]->sensorless_estimator_.error_ >> 16;
			tx_data[3] = axes[0]->sensorless_estimator_.error_ >> 24;
			tx_data[4] = axes[1]->sensorless_estimator_.error_;
			tx_data[5] = axes[1]->sensorless_estimator_.error_ >> 8;
			tx_data[6] = axes[1]->sensorless_estimator_.error_ >> 16;
			tx_data[7] = axes[1]->sensorless_estimator_.error_ >> 24;

			canSend();

			/* Diagnostics */

			tx_header.StdId = 0xF0;
			tx_header.DLC = 8;

			int16_t velEstimateInt;

			velEstimateInt = axes[0]->sensorless_estimator_.vel_estimate_;
			tx_data[0] = velEstimateInt;
			tx_data[1] = velEstimateInt >> 8;
			tx_data[2] = 0;
			tx_data[3] = 0;

			velEstimateInt = axes[1]->sensorless_estimator_.vel_estimate_;
			tx_data[0] = velEstimateInt;
			tx_data[1] = velEstimateInt >> 8;
			tx_data[2] = 0;
			tx_data[3] = 0;


			canSend();



		}
	}
}

// Necessary to call this funtion from c code

void server_wrapper(void *arg)
{
	//uart_print("start server\n");
	reinterpret_cast<CAN_Interface*>(arg)->server_thread();

	for(;;)
	{
		//uart_print("server\n");
		osDelay(200);
	}


}


// Convenience Function
void CAN_Interface::canSend()
{
	while(HAL_CAN_GetTxMailboxesFreeLevel(canHandle) == 0)
	{
		osDelay(1);
	}
	HAL_CAN_AddTxMessage(canHandle, &tx_header, tx_data, &txMailbox);
}

void transmitter_wrapper(void *arg)
{
	reinterpret_cast<CAN_Interface*>(arg)->transmitter_thread();
}

void CAN_Interface::start_threads()
{
	//uart_print("threadstarter\n");
	osThreadDef(server_thread_def,(os_pthread) server_wrapper, server_priority, 0, 4000);
	server_thread_id = osThreadCreate(osThread(server_thread_def), this);

	osThreadDef(transmitter_thread_def, (os_pthread)transmitter_wrapper, transmitter_priority, 0, 1000);
	transmitter_thread_id = osThreadCreate(osThread(transmitter_thread_def), this);

	//uart_print("threadstarter end\n");


}

void CAN_Interface::RxCallback(CAN_HandleTypeDef *hcan, uint32_t rxFifo)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	/* Notify the task that a can message has been received */

	HAL_CAN_GetRxMessage(hcan, rxFifo, &rx_header, rx_data);

	vTaskNotifyGiveFromISR(server_thread_id, &xHigherPriorityTaskWoken);

	/* if xHigherPriorityTaskWoken is set to pdTrue then a context switch is performed */
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken)
}


void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	if(can_interface.canHandle == hcan)
	{
		can_interface.RxCallback(hcan, CAN_RX_FIFO0);
	}
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	if(can_interface.canHandle == hcan)
	{
		can_interface.RxCallback(hcan, CAN_RX_FIFO0);
	}
}

