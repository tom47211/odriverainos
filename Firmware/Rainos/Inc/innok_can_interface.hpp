/*
 * innok_can_interface.hpp
 *
 *  Created on: 14.10.2019
 *      Author: thomas
 */

#ifndef INNOK_INC_INNOK_CAN_INTERFACE_HPP_
#define INNOK_INC_INNOK_CAN_INTERFACE_HPP_

#include "can.h"
#include "cmsis_os.h"

class CAN_Interface
{
private:
	void set_filter();
	void canSend();

public:

	CAN_HandleTypeDef *canHandle = &hcan1;

	CAN_RxHeaderTypeDef rx_header;
	CAN_TxHeaderTypeDef tx_header;

	uint8_t rx_data[8];
	uint8_t tx_data[8];

	uint32_t txMailbox;

	//Threads

	osPriority server_priority = osPriorityHigh;
	osThreadId server_thread_id = NULL;
	void server_thread();

	osPriority transmitter_priority = osPriorityBelowNormal;
	osThreadId transmitter_thread_id = NULL;
	void transmitter_thread();

	void start_threads();


	void RxCallback(CAN_HandleTypeDef*, uint32_t rxFifo);

	//Constructor
	CAN_Interface()
	{
	}
	CAN_Interface(CAN_HandleTypeDef *hcan) :
			canHandle(hcan)
	{
	}
};

#endif /* INNOK_INC_INNOK_CAN_INTERFACE_HPP_ */
