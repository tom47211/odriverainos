/*
 * pump.cpp
 *
 *  Created on: 16.03.2020
 *      Author: thomas
 */

#include "pump.hpp"



/*
 * vel = 0:    targetVel == velMin
 * vel = 0xFF: targetVel == velMax
 */

void Pump::cmdReceived(uint8_t cmd, uint8_t vel)
{
	lastCommandTimestamp = osKernelSysTick();
	if(cmd == 0)
	{
		targetVel = 0;
	}
	else if(cmd == 1)
	{
		targetVel = config->velMin + ((config->velMax-config->velMin) * vel)/0xFF;
	}
	else if(cmd == 2)
	{
		resetDryRunProtection();
	}
	else if(cmd == 3)
	{
		resetError();
	}
}

void Pump::stop()
{
	targetVel = 0;
	p_axis->requested_state_ = Axis::AXIS_STATE_IDLE;
}


void Pump::resetDryRunProtection()
{
	dryRunResetFlag = 1;
}

void Pump::resetError()
{
	errorResetFlag = 1;
}

void Pump::loop()
{
	switch(state)
	{
	case Idle:
		if(targetVel != 0 && (osKernelSysTick() - lastCommandTimestamp) < config->timeout)
		{
			state = Running;
			p_axis->controller_.vel_setpoint_ = targetVel;
			p_axis->requested_state_=Axis::AXIS_STATE_SENSORLESS_CONTROL;
		}
		break;
	case Running:
		if(p_axis->error_)
		{
			state = Error;
		}
		else if( targetVel == 0 ||
				(osKernelSysTick() - lastCommandTimestamp) < config->timeout ||
				p_axis->current_state_ != Axis::AXIS_STATE_CLOSED_LOOP_CONTROL ||
				p_axis->current_state_ != Axis::AXIS_STATE_LOCKIN_SPIN ||
				p_axis->current_state_ != Axis::AXIS_STATE_SENSORLESS_CONTROL )
		{
			targetVel = 0;
			p_axis->requested_state_ = Axis::AXIS_STATE_IDLE;
			state = Idle;
		}

		else
		{
			if (p_axis->motor_.current_control_.Iq_measured	< config->iqTripLevel)
			{
				iqTripCounter++;
			}
			else
			{
				iqTripCounter--;
			}

			if(iqTripCounter > config->maxIqTrips)
			{
				targetVel = 0;
				p_axis->requested_state_= Axis::AXIS_STATE_IDLE;
				state=DryRunProtection;
			}
			else
			{
				p_axis->controller_.vel_setpoint_ = targetVel;
			}
		}


		break;
	case DryRunProtection:
		if(dryRunResetFlag)
		{
			targetVel = 0;
			state = Idle;
		}
		break;
	case Error:
		if(errorResetFlag)
		{
			p_axis->error_ = Axis::ERROR_NONE;
			p_axis->controller_.error_ = Controller::ERROR_NONE;
			p_axis->sensorless_estimator_.error_ = SensorlessEstimator::ERROR_NONE;
			p_axis->motor_.error_ = Motor::ERROR_NONE;
			targetVel = 0;
			state = Idle;
		}
		break;
	}

	dryRunResetFlag = 0;
	errorResetFlag = 0;
}
