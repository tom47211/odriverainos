/*
 * globals.h
 *
 *  Created on: 24.09.2018
 *      Author: thomas
 */

#ifndef EEPROM_VARIABLES_H_
#define EEPROM_VARIABLES_H_


#include "stm32f4xx.h"

#define EE_ADDR_NODE_ID		0x0001
#define EE_ADDR_LENGTH_HIGH 0x0002
#define EE_ADDR_LENGTH_LOW	0x0003
#define EE_ADDR_BOOT_PW		0x0004
#define EE_ADDR_CRC_HIGH	0x0005
#define EE_ADDR_CRC_LOW		0x0006


#define NumbOfVar 12
extern uint16_t VirtAddVarTab[NumbOfVar];



#endif /* EEPROM_VARIABLES_H_ */
