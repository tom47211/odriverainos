/*
 * CANopenDictionary.c
 *
 *  Created on: 07.08.2019
 *      Author: thomas
 */

#include "CANopenDictionary.hpp"
#include "eeprom_emulation.h"
#include "eeprom_emulation_variables.h"
#include "cmsis_os.h"
#include "string.h"
#include "usart.h"
#include "can.h"
#include "nvm_config.hpp"
#include "odrive_main.h"
#include "pump.hpp"


CANopenDictionary::CANopenDictionary(CANopenDictionaryEntry_t *_entries, uint32_t _max_size) :
		entries(_entries), max_size(_max_size)
{
}

int32_t CANopenDictionary::getArrayIndex(uint16_t index, uint8_t subindex)
{
	for (int i = 0; i < current_size; i++)
	{
		if (entries[i].index == index)
		{
			if (entries[i].subindex == subindex)
			{
				return i;
			}
		}
	}
	return -1;
}

void CANopenDictionary::addEntry(uint16_t _index, uint8_t _subindex, datatype_t _datatype, void *_value , accessType_t _accesType, uint32_t _size = 0, uint8_t (*_function)() = NULL,
		void *_argument = NULL, int64_t _min_value = 0,
		int64_t _max_value = 0)
{
	switch (_datatype) {
		case u_int_8:
		case int_8:
		case boolean:
			entries[current_size].size = 1;
			break;

		case u_int_16:
		case int_16:
			entries[current_size].size = 2;
			break;

		case u_int_32:
		case int_32:
		case float_ieee:
		case float_factor_10:
		case float_factor_100:
		case float_factor_1000:
			entries[current_size].size = 4;
			break;

		case u_int_64:
		case int_64:
		case double_ieee:
			entries[current_size].size = 8;
			break;

		case string:
			if(_size == 0)
			{
				entries[current_size].size = strlen((char*)_value);
			}
			else
			{
				entries[current_size].size = _size;
			}
			break;

		case datablock:
			entries[current_size].size = _size;
			break;
		default:
			entries[current_size].size = _size;

			break;
	}

	entries[current_size].index = _index;
	entries[current_size].subindex = _subindex;
	entries[current_size].value = _value;
	entries[current_size].function = _function;
	entries[current_size].argument = _argument;
	entries[current_size].datatype = _datatype;
	entries[current_size].accessType = _accesType;
	entries[current_size].min_value = _min_value;
	entries[current_size].max_value = _max_value;

	current_size++;
}

void CANopenDictionary::addFunction(uint16_t _index, uint8_t _subindex, uint8_t (*_function)(), void *_argument = NULL)
{
	entries[current_size].index = _index;
	entries[current_size].subindex = _subindex;
	entries[current_size].value = NULL;
	entries[current_size].size = 0;
	entries[current_size].function = _function;
	entries[current_size].argument = _argument;
	entries[current_size].datatype = none;
	entries[current_size].accessType = write_only;
	entries[current_size].min_value = 0;
	entries[current_size].max_value = 0;

	current_size++;
}

// Dictionary function prototypes
extern "C" int load_configuration(void);

uint8_t calibrationWrapper();
uint8_t erase_configurationWrapper();
uint8_t OD_storeParameter(void);
uint8_t OD_restoreDefaultParameter();
uint8_t DFU_Initiate();

volatile_SDO_t volatile_SDO;

typedef Config<BoardConfig_t, Encoder::Config_t[AXIS_COUNT], SensorlessEstimator::Config_t[AXIS_COUNT], Controller::Config_t[AXIS_COUNT], Motor::Config_t[AXIS_COUNT], TrapezoidalTrajectory::Config_t[AXIS_COUNT], Axis::Config_t[AXIS_COUNT]> ConfigFormat;

extern BoardConfig_t board_config;
extern Encoder::Config_t encoder_configs[AXIS_COUNT];
extern SensorlessEstimator::Config_t sensorless_configs[AXIS_COUNT];
extern Controller::Config_t controller_configs[AXIS_COUNT];
extern Motor::Config_t motor_configs[AXIS_COUNT];
extern Axis::Config_t axis_configs[AXIS_COUNT];
extern TrapezoidalTrajectory::Config_t trap_configs[AXIS_COUNT];
extern Pump::Config_t pump_configs[AXIS_COUNT];


CANopenDictionaryEntry_t entry_array[DICTIONARY_MAX_SIZE] =  {0};


CANopenDictionary dictionary(entry_array, sizeof(entry_array) / sizeof(CANopenDictionaryEntry_t));


extern bool requestReset;

void init_dictionary(CANopenDictionary *dictionary)
{
	uint16_t index;
	uint8_t subindex;

	/*
	 * General Functions and Parameter
	 * 0x2000
	 */

	index=0x2000;
	subindex=0x0;
	dictionary->addFunction(index, subindex++, &calibrationWrapper);
	dictionary->addEntry(index, subindex++, u_int_32, &volatile_SDO.storeParameter_password, write_only, 4, &OD_storeParameter);
	dictionary->addFunction(index, subindex++, &erase_configurationWrapper);
	dictionary->addEntry(index, subindex++, u_int_8, &volatile_SDO.dfu_initiate_password, write_only, 0, &DFU_Initiate);



	/*
	 * Pumps Config
	 * 0x2100
	 *
	 */
	index = 0x2100;
	subindex = 0;
	/* 0x0 */
	dictionary->addEntry(index, subindex++, u_int_8, (void*) &volatile_SDO.node_id,
			read_write);
//	/* 0x1 */
//	dictionary->addEntry(index, subindex++, u_int_8, (void*) &robot->config->drivemode,
//			read_write);
//	/* 0x2 */
//	dictionary->addEntry(index, subindex++, u_int_8, (void*) &robot->config->direction,
//			read_write);
//	/* 0x3 */
//	dictionary->addEntry(index, subindex++, u_int_16, (void*) &robot->config->gearratio,
//			read_write);
//	/* 0x4 */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->max_rpm,
//			read_write);
//	/* 0x5 */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->min_rpm,
//			read_write);
//	/* 0x6 */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->speed_limit.fwd,
//			read_write);
//	/* 0x7 */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->speed_limit.bwd,
//			read_write);
//	/* 0x8 */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->speed_limit.side,
//			read_write);
//	/* 0x9 */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->speed_limit.yaw,
//			read_write);
//	/* 0xA */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->tiresize,
//			read_write);
//	/* 0xB */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->track_gauge,
//			read_write);
//	/* 0xC */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->track_length,
//			read_write);
//	/* 0xD */
//	dictionary->addEntry(index, subindex++, float_ieee, (void*) &robot->config->pivot_shift,
//			read_write);
//	/* 0xE */
//	dictionary->addEntry(index,subindex++, u_int_32, (void*) &robot->config->operating_time_vehicle,
//			read_write);
//	/* 0xF */
//	dictionary->addEntry(index,subindex++, u_int_32, (void*) &robot->config->operating_time_motors,
//			read_write);

	/*
	 * Robot
	 * 0x2110
	 *
	 */

	index = 0x2110;
	subindex = 0;
	/* 0x0 */
//	dictionary->addEntry(index, subindex++, u_int_8, (void*) &robot->ctrl_flags.request_idle,
//			read_only);






	index = 0x2400;
	subindex = 0;

	dictionary->addEntry(index, subindex, float_ieee, (void *)&board_config.brake_resistance, read_write);
	subindex++;
	dictionary->addEntry(index, subindex, float_ieee, (void *)&board_config.dc_bus_overvoltage_trip_level, read_write);
	subindex++;
	dictionary->addEntry(index, subindex, float_ieee, (void *)&board_config.dc_bus_undervoltage_trip_level, read_write);




	for (int i = 0; i < AXIS_COUNT; i++) {

		uint16_t axis_offset = i*0x100;

		/*
		 * Axis Config:
		 * 0x3i00
		 *
		 */

		index = 0x3000 + axis_offset;
		subindex = 0;
		/* 0x0 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &axes[i]->config_.startup_closed_loop_control,
				read_write);
		/* 0x1 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &axes[i]->config_.startup_encoder_index_search,
				read_write);

		/*
		 * Sensorless Ramp Config (subconfig of axis config)
		 *
		 */

		Axis::LockinConfig_t *sensorless_config = &axes[i]->config_.sensorless_ramp;

		/* 0x2 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &sensorless_config->accel,
				read_write);

		/* 0x3 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &sensorless_config->current,
				read_write);

		/* 0x4 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &sensorless_config->ramp_distance,
				read_write);

		/* 0x5 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &sensorless_config->ramp_time,
				read_write);

		/* 0x6 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &sensorless_config->vel,
				read_write);

		/* 0x7 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &sensorless_config->finish_on_distance,
				read_write);

		/* 0x8 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &sensorless_config->finish_on_distance,
				read_write);

		/* 0x9 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &sensorless_config->finish_on_vel,
				read_write);

		/* 0xA */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &sensorless_config->finish_on_enc_idx,
				read_write);

		/*
		 * Axis Parameter
		 * 0x3i01
		 *
		 */
		index = 0x3001 + axis_offset;
		subindex = 0;
		/* 0x0 */
		dictionary->addEntry(index, subindex++, u_int_32,
				(void*) &axes[i]->current_state_,
				read_write);
		/* 0x1 */
		dictionary->addEntry(index, subindex++, u_int_32,
				(void*) &axes[i]->error_,
				read_write);

		/*
		 * Motor Config
		 * 0x3i10
		 */

		Motor::Config_t *motor_cfg = &axes[i]->motor_.config_;

		index = 0x3010 + axis_offset;
		subindex = 0;
		/* 0x0 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &motor_cfg->pre_calibrated,
				read_write);

		/* 0x1 */
		dictionary->addEntry(index, subindex++, int_32,
				(void*) &motor_cfg->pole_pairs,
				read_write);

		/* 0x2 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->calibration_current,
				read_write);

		/* 0x3 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->resistance_calib_max_voltage,
				read_write);

		/* 0x4 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->phase_inductance,
				read_write);

		/* 0x5 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->phase_resistance,
				read_write);

		/* 0x6 */
		dictionary->addEntry(index, subindex++, int_32,
				(void*) &motor_cfg->direction,
				read_write);

		/* 0x7 */
		dictionary->addEntry(index, subindex++, u_int_8,
				(void*) &motor_cfg->motor_type,
				read_write);

		/* 0x8 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->current_lim,
				read_write);
		/* 0x9 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->current_lim_tolerance,
				read_write);

		/* 0xA */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->requested_current_range,
				read_write);

		/* 0xB */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->current_control_bandwidth,
				read_write);

		/* 0xC */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->inverter_temp_limit_lower,
				read_write);

		/* 0xD */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &motor_cfg->inverter_temp_limit_upper,
				read_write);

		/*
		 * Motor Parameter
		 * 0x3i11
		 */
		index = 0x3011 + axis_offset;
		subindex = 0;
		/* 0x0 */
		dictionary->addEntry(index, subindex++, boolean, (void*) &axes[i]->motor_.is_calibrated_, read_only);
		/* 0x1 */
		dictionary->addEntry(index, subindex++, boolean, (void*) &axes[i]->motor_.armed_state_, read_only);




		/*
		 * Encoder
		 * 0x3i20
		 */

		Encoder::Config_t *encoder_cfg =
				&axes[i]->encoder_.config_;

		index = 0x3020 + axis_offset;
		subindex = 0;
		/* 0x0 */
		dictionary->addEntry(index, subindex++, u_int_8,
				(void*) &encoder_cfg->mode, read_write);
		/* 0x1 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &encoder_cfg->use_index, read_write);
		/* 0x2 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &encoder_cfg->pre_calibrated, read_write);
		/* 0x3 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &encoder_cfg->zero_count_on_find_idx, read_write);
		/* 0x4 */
		dictionary->addEntry(index, subindex++, int_32,
				(void*) &encoder_cfg->cpr, read_write);
		/* 0x5 */
		dictionary->addEntry(index, subindex++, int_32,
				(void*) &encoder_cfg->offset, read_write);
		/* 0x6 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &encoder_cfg->offset_float, read_write);
		/* 0x7 */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &encoder_cfg->enable_phase_interpolation, read_write);
		/* 0x8 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &encoder_cfg->calib_range, read_write);
		/* 0x9 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &encoder_cfg->calib_scan_distance, read_write);
		/* 0xA */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &encoder_cfg->calib_scan_omega, read_write);
		/* 0xB */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &encoder_cfg->bandwidth, read_write);
		/* 0xC */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &encoder_cfg->find_idx_on_lockin_only, read_write);
		/* 0xD */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &encoder_cfg->idx_search_unidirectional, read_write);
		/* 0xE */
		dictionary->addEntry(index, subindex++, boolean,
				(void*) &encoder_cfg->ignore_illegal_hall_state, read_write);


		index = 0x3021 + axis_offset;
		subindex = 0;
		/* 0x0 */
		dictionary->addEntry(index, subindex++, int_32,
				(void*) &axes[i]->encoder_.count_in_cpr_, read_only);
		/* 0x1 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &axes[i]->encoder_.pos_cpr_, read_only);
		/* 0x2 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &axes[i]->encoder_.pos_estimate_, read_only);
		/* 0x3 */
		dictionary->addEntry(index, subindex++, int_32,
				(void*) &axes[i]->encoder_.shadow_count_, read_only);

		/*
		 * Controller Config
		 * 0x3i30
		 */

		Controller::Config_t *controller_cfg =
				&axes[i]->controller_.config_;


		index = 0x3030 + axis_offset;
		subindex = 0;				//TODO which size do enums have?
		/* 0x0 */
		dictionary->addEntry(index, subindex++, u_int_8,
				(void*) &controller_cfg->control_mode,
				read_write);
		/* 0x1 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &controller_cfg->pos_gain,
				read_write);
		/* 0x2 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &controller_cfg->vel_gain,
				read_write);
		/* 0x3 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &controller_cfg->vel_integrator_gain,
				read_write);
		/* 0x4 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &controller_cfg->vel_limit,
				read_write);
		/* 0x5 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &controller_cfg->vel_limit_tolerance,
				read_write);
		/* 0x6 */
		dictionary->addEntry(index, subindex++, float_ieee,
				(void*) &controller_cfg->vel_ramp_rate,
				read_write);
		/* 0x7 */
		dictionary->addEntry(index, subindex, boolean,
				(void*) &controller_cfg->setpoints_in_cpr,
				read_write);

		/*
		 * Controller Parameter
		 * 0x3i31
		 */
		index = 0x3031 + axis_offset;
		dictionary->addEntry(index, subindex, float_ieee,
						static_cast<void *>(&axes[i]->controller_.vel_ramp_target_),
						read_only);

		/*
		 *
		 *	Sensorless Estimator Config
		 */

		SensorlessEstimator::Config_t *estimator_config = &axes[i]->sensorless_estimator_.config_;

		index = 0x3040 + axis_offset;

		dictionary->addEntry(index,subindex++, u_int_32, static_cast<void*>(&estimator_config->observer_gain),read_write, 4,NULL,NULL,0,0);



		/*
		 * Pump Config
		 * 0x3i40
		 */
		Pump::Config_t *pump_cfg = &pump_configs[i];
		index = 0x3050 + axis_offset;
		dictionary->addEntry(index,subindex++, u_int_32, static_cast<void*>(&pump_cfg->timeout),read_write, 4,NULL,NULL,0,0);
		dictionary->addEntry(index,subindex++, u_int_32, static_cast<void*>(&pump_cfg->maxIqTrips),read_write, 4,NULL,NULL,0,0);
		dictionary->addEntry(index,subindex++, float_ieee, static_cast<void*>(&pump_cfg->iqTripLevel),read_write, 4,NULL,NULL,0,0);
		dictionary->addEntry(index,subindex++, float_ieee, static_cast<void*>(&pump_cfg->velMax),read_write, 4,NULL,NULL,0,0);
		dictionary->addEntry(index,subindex++, float_ieee, static_cast<void*>(&pump_cfg->velMin),read_write, 4,NULL,NULL,0,0);

	}
}

uint8_t calibrationWrapper()
{
	// TODO robot->requestMotorCalibration();
	return 0;
}



uint8_t erase_configurationWrapper()
{
	if(0)//TODO state != Robot::Idle)
	{
		return 2;
	}

	erase_configuration();

	requestReset = true;
	return 0;
}

uint8_t requestResetWrapper()
{
	requestReset = true;
}

uint8_t DFU_Initiate()
{

	if (volatile_SDO.dfu_initiate_password == 0xFF)
	{
		//print("Initiate device firmware upgrade\n");

		//TODO flash mutex  xSemaphoreTake(FlashMutexHandle, 1000);

		EE_WriteVariable(EE_ADDR_BOOT_PW, 0x0000);

		//TODO xSemaphoreGive(FlashMutexHandle);

		requestReset = true;
		return 0;
	}
	else
	{
		return 1;
	}
}

uint8_t OD_restoreDefaultParameter()
{
	uint8_t return_val;

	//ASCII "load"
	if (volatile_SDO.restoreDefaultParameter_password == 0x64616F6C)
	{

		/*
		 * TODO: restore defaults
		 */

		board_config = BoardConfig_t();
		for (size_t i = 0; i < AXIS_COUNT; ++i)
		{
			encoder_configs[i] = Encoder::Config_t();
			sensorless_configs[i] = SensorlessEstimator::Config_t();
			controller_configs[i] = Controller::Config_t();
			motor_configs[i] = Motor::Config_t();
			trap_configs[i] = TrapezoidalTrajectory::Config_t();
			axis_configs[i] = Axis::Config_t();
			// Default step/dir pins are different, so we need to explicitly load them
			Axis::load_default_step_dir_pin_config(hw_configs[i].axis_config, &axis_configs[i]);
		}


		volatile_SDO.restoreDefaultParameter_password = 0;

		return_val = 1;

	}
	else
	{
		return_val = 0;
	}

	return return_val;
}

uint8_t OD_storeParameter()
{

	//ASCII "save"
	if (volatile_SDO.storeParameter_password == 0x65766173)
	{
		volatile_SDO.storeParameter_password = 0;
		if(0)//TODO robot->state != Robot::Idle)
		{
			return 2;
		}

		user_config_loaded_=false;
		save_configuration();

		if(user_config_loaded_)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 1;
	}
}



