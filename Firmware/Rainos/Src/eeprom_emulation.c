/**
 ******************************************************************************
 * @file    EEPROM_Emulation/src/eeprom.c
 * @author  MCD Application Team
 * @version V3.1.0
 * @date    07/27/2009
 * @brief   This file provides all the EEPROM emulation firmware functions.
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
 */
/** @addtogroup EEPROM_Emulation
 * @{
 */

/* Includes ------------------------------------------------------------------*/
#include "eeprom_emulation.h"
#include "eeprom_emulation_variables.h"
#include "stm32f4xx_hal.h"
#include <stdio.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define HAL_FLASH_ClearError() __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR | FLASH_FLAG_PGPERR)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


eeprom_emulation ee = {
		EE_UNINITIALIZED,

		.sectors = {
				{
						.nbr = FLASH_SECTOR_1,
						.base_addr = SECTOR1_BASE,
						.end_addr = SECTOR1_END,
						.status = SECTOR_ERASED,
				},
				{
						.nbr = FLASH_SECTOR_2,
						.base_addr = SECTOR2_BASE,
						.end_addr = SECTOR2_END,
						.status = SECTOR_ERASED,
				}
		}
};

/* Global variable used to store variable value in read sequence */
uint16_t DataVar = 0;

/* Virtual address defined by the user: 0xFFFF value is prohibited */

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

//static FLASH_Status EE_Format(void);
static uint16_t EE_FindValidSector(uint8_t Operation);
static uint16_t EE_VerifySectorFullWriteVariable(uint16_t VirtAddress, uint16_t Data);
static uint16_t EE_SectorTransfer(uint16_t VirtAddress, uint16_t Data);
static void EE_SectorReadStatus(uint16_t SectorNumber);

static void EE_SectorReadStatus(uint16_t SectorNumber)
{
	ee.sectors[SectorNumber].status = (*(__IO uint16_t*) ee.sectors[SectorNumber].base_addr);
}

/**
 * @brief  Restore the sectors to a known good state in case of sector's status
 *   corruption after a power loss.
 * @param  None.
 * @retval - Flash error code: on write Flash error
 *         - FLASH_COMPLETE: on success
 */
uint16_t EE_Init(void)
{

	uint16_t VarIdx = 0;
	uint16_t EepromStatus = 0, ReadStatus = 0;
	int16_t x = -1;
	uint32_t FlashStatus;

	uint32_t return_status = 0;

	uint32_t FlashError;
	FLASH_EraseInitTypeDef sector_erase;

	sector_erase.TypeErase = FLASH_TYPEERASE_SECTORS;
	sector_erase.NbSectors = 1;
	sector_erase.VoltageRange = FLASH_VOLTAGE_RANGE_3;

	/* Get Sector0 status */
	EE_SectorReadStatus(SECTOR0);
	/* Get Sector1 status */
	EE_SectorReadStatus(SECTOR1);

	/* Check for invalid header states and repair if necessary */
	switch (ee.sectors[0].status)
	{
		case SECTOR_ERASED:
			if (ee.sectors[1].status == VALID_SECTOR) /* Sector0 SECTOR_ERASED, Sector1 valid */
			{
				/* Erase Sector0 */
				sector_erase.Sector = ee.sectors[0].nbr;
				HAL_FLASH_Unlock();
			    HAL_FLASH_ClearError();
				FlashStatus = HAL_FLASHEx_Erase(&sector_erase, &FlashError);
				HAL_FLASH_Lock();
				/* If erase operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}

			else if (ee.sectors[1].status == RECEIVE_DATA) /* Sector0 SECTOR_ERASED, Sector1 receive */
			{
				/* Erase Sector0 */
				sector_erase.Sector = ee.sectors[0].nbr;
				HAL_FLASH_Unlock();
			    HAL_FLASH_ClearError();
				FlashStatus = HAL_FLASHEx_Erase(&sector_erase, &FlashError);
				HAL_FLASH_Lock();
				/* If erase operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
				/* Mark Sector1 as valid */
				FlashStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, ee.sectors[1].base_addr, (uint16_t) VALID_SECTOR);
				/* If program operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}
			else /* First EEPROM access (Sector0&1 are SECTOR_ERASED) or invalid state -> format EEPROM */
			{
				/* Erase both Sector0 and Sector1 and set Sector0 as valid sector */
				FlashStatus = EE_Format();
				/* If erase/program operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}
			break;

		case RECEIVE_DATA:
			if (ee.sectors[1].status == VALID_SECTOR) /* Sector0 receive, Sector1 valid */
			{
				/* Transfer data from Sector1 to Sector0 */
				for (VarIdx = 0; VarIdx < NumbOfVar; VarIdx++)
				{
					if ((*(__IO uint16_t*) (ee.sectors[0].base_addr + 6)) == VirtAddVarTab[VarIdx])
					{
						x = VarIdx;
					}
					if (VarIdx != x)
					{
						/* Read the last variables' updates */
						ReadStatus = EE_ReadVariable(VirtAddVarTab[VarIdx], &DataVar);
						/* In case variable corresponding to the virtual address was found */
						if (ReadStatus != 0x1)
						{
							/* Transfer the variable to the Sector0 */
							EepromStatus = EE_VerifySectorFullWriteVariable(VirtAddVarTab[VarIdx], DataVar);
							/* If program operation was failed, a Flash error code is returned */
							if (EepromStatus != FLASH_COMPLETE)
							{
								return_status =  EepromStatus;
							}
						}
					}
				}
				/* Mark Sector0 as valid */
				FlashStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, ee.sectors[0].base_addr, (uint16_t) VALID_SECTOR);

				/* If program operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
				/* Erase Sector1 */
				sector_erase.Sector = ee.sectors[1].nbr;
				HAL_FLASH_Unlock();
			    HAL_FLASH_ClearError();
				FlashStatus = HAL_FLASHEx_Erase(&sector_erase, &FlashError);
				HAL_FLASH_Lock();

				/* If erase operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}
			else if (ee.sectors[1].status == SECTOR_ERASED) /* Sector0 receive, Sector1 SECTOR_ERASED */
			{
				/* Erase Sector1 */
				sector_erase.Sector = ee.sectors[1].nbr;
				HAL_FLASH_Unlock();
			    HAL_FLASH_ClearError();
				FlashStatus = HAL_FLASHEx_Erase(&sector_erase, &FlashError);
				HAL_FLASH_Lock();
				/* If erase operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
				/* Mark Sector0 as valid */
				HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, ee.sectors[0].base_addr, VALID_SECTOR);

				/* If program operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}
			else /* Invalid state -> format eeprom */
			{
				/* Erase both Sector0 and Sector1 and set Sector0 as valid sector */
				FlashStatus = EE_Format();
				/* If erase/program operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}
			break;

		case VALID_SECTOR:
			if (ee.sectors[1].status == VALID_SECTOR) /* Invalid state -> format eeprom */
			{
				/* Erase both Sector0 and Sector1 and set Sector0 as valid sector */
				FlashStatus = EE_Format();
				/* If erase/program operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}
			else if (ee.sectors[1].status == SECTOR_ERASED) /* Sector0 valid, Sector1 SECTOR_ERASED */
			{
				/* Erase Sector1 */
				sector_erase.Sector = ee.sectors[1].nbr;
				HAL_FLASH_Unlock();
			    HAL_FLASH_ClearError();
				FlashStatus = HAL_FLASHEx_Erase(&sector_erase, &FlashError);
				HAL_FLASH_Lock();

				/* If erase operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}
			else /* Sector0 valid, Sector1 receive */
			{
				/* Transfer data from Sector0 to Sector1 */
				for (VarIdx = 0; VarIdx < NumbOfVar; VarIdx++)
				{
					if ((*(__IO uint16_t*) (ee.sectors[1].base_addr + 6)) == VirtAddVarTab[VarIdx])
					{
						x = VarIdx;
					}
					if (VarIdx != x)
					{
						/* Read the last variables' updates */
						ReadStatus = EE_ReadVariable(VirtAddVarTab[VarIdx], &DataVar);
						/* In case variable corresponding to the virtual address was found */
						if (ReadStatus != 0x1)
						{
							/* Transfer the variable to the Sector1 */
							EepromStatus = EE_VerifySectorFullWriteVariable(VirtAddVarTab[VarIdx], DataVar);
							/* If program operation was failed, a Flash error code is returned */
							if (EepromStatus != FLASH_COMPLETE)
							{
								return_status =  EepromStatus;
							}
						}
					}
				}
				/* Mark Sector1 as valid */
				FlashStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, ee.sectors[1].base_addr, VALID_SECTOR);

				/* If program operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
				/* Erase Sector0 */
				sector_erase.Sector = ee.sectors[0].nbr;

				HAL_FLASH_Unlock();
			    HAL_FLASH_ClearError();
				FlashStatus = HAL_FLASHEx_Erase(&sector_erase, &FlashError);
				HAL_FLASH_Lock();
				/* If erase operation was failed, a Flash error code is returned */
				if (FlashStatus != FLASH_COMPLETE)
				{
					return_status =  FlashStatus;
				}
			}
			break;

		default: /* Any other state -> format eeprom */
			/* Erase both Sector0 and Sector1 and set Sector0 as valid sector */
			FlashStatus = EE_Format();
			/* If erase/program operation was failed, a Flash error code is returned */
			if (FlashStatus != FLASH_COMPLETE)
			{
				return_status =  FlashStatus;
			}
			break;
	}

	/* Get Sector0 status */
	EE_SectorReadStatus(SECTOR0);
	/* Get Sector1 status */
	EE_SectorReadStatus(SECTOR1);

	if (return_status != FLASH_COMPLETE)
	{
		ee.state = EE_ERROR;
	}
	else
	{
		ee.state = EE_INITIALIZED;
	}

	return return_status;
}

/**
 * @brief  Returns the last stored variable data, if found, which correspond to
 *   the passed virtual address
 * @param  VirtAddress: Variable virtual address
 * @param  Data: Global variable contains the read variable value
 * @retval Success or error status:
 *           - 0: if variable was found
 *           - 1: if the variable was not found
 *           - 2: if eeprom emulation state is not EE_INITIALIZED
 *           - NO_VALID_SECTOR: if no valid sector was found.
 */
uint16_t EE_ReadVariable(uint16_t VirtAddress, uint16_t *Data)
{
	if(ee.state != EE_INITIALIZED)
	{
		return 2;
	}
	uint16_t ValidSector = 0;
	uint16_t AddressValue = 0x5555, ReadStatus = 1;

	/* Get active Sector for read operation */
	ValidSector = EE_FindValidSector(READ_FROM_VALID_SECTOR);

	/* Check if there is no valid sector */
	if (ValidSector == NO_VALID_SECTOR)
	{
		return NO_VALID_SECTOR;
	}

	/* Get the valid Sector start Address */

	uint32_t CurrentAddress = ee.sectors[ValidSector].end_addr - 1;

	/* Check each active sector address starting from end */
	while (CurrentAddress > (ee.sectors[ValidSector].base_addr + 2))
	{
		/* Get the current location content to be compared with virtual address */
		AddressValue = (*(__IO uint16_t*) CurrentAddress);

		/* Compare the read address with the virtual address */

		if (AddressValue == VirtAddress)
		{
			/* Get content of Address-2 which is variable value */
			*Data = (*(__IO uint16_t*) (CurrentAddress - 2));

			/* In case variable value is read, reset ReadStatus flag */
			ReadStatus = 0;

			break;
		}
		else
		{
			/* Next address location */
			CurrentAddress = CurrentAddress - 4;
		}
	}

	/* Return ReadStatus value: (0: variable exist, 1: variable doesn't exist) */
	return ReadStatus;
}

/**
 * @brief  Writes/upadtes variable data in EEPROM.
 * @param  VirtAddress: Variable virtual address
 * @param  Data: 16 bit data to be written
 * @retval Success or error status:
 *           - FLASH_COMPLETE: on success
 *           - SECTOR_FULL: if valid sector is full
 *           - NO_VALID_SECTOR: if no valid sector was found
 *           - Flash error code: on write Flash error
 */
uint16_t EE_WriteVariable(uint16_t VirtAddress, uint16_t Data)
{
	if(ee.state != EE_INITIALIZED)
	{
		return 2;
	}

	uint16_t tempData;
	uint16_t readStatus = EE_ReadVariable(VirtAddress,&tempData);

	if(readStatus == 0 && tempData == Data)
	{
		return FLASH_COMPLETE;
	}


	uint16_t Status = 0;

	/* Write the variable virtual address and value in the EEPROM */
	Status = EE_VerifySectorFullWriteVariable(VirtAddress, Data);

	/* In case the EEPROM active sector is full */
	if (Status == SECTOR_FULL)
	{
		/* Perform Sector transfer */
		Status = EE_SectorTransfer(VirtAddress, Data);
	}

	/* Return last operation status */
	return Status;
}

/**
 * @brief  Erases SECTOR0 and SECTOR1 and writes VALID_SECTOR header to SECTOR0
 * @param  None
 * @retval Status of the last operation (Flash write or erase) done during
 *         EEPROM formating
 */
uint16_t EE_Format(void)
{
	uint32_t flash_status;
	FLASH_EraseInitTypeDef eeprom_format;
	eeprom_format.NbSectors = 2;
	eeprom_format.TypeErase = FLASH_TYPEERASE_SECTORS;
	eeprom_format.Sector = ee.sectors[0].nbr;
	eeprom_format.VoltageRange = FLASH_VOLTAGE_RANGE_3;
	HAL_FLASH_Unlock();
	HAL_FLASH_ClearError();
	HAL_FLASHEx_Erase(&eeprom_format, &flash_status);
	HAL_FLASH_Lock();
	if (flash_status != 0xFFFFFFFF)
	{
		return flash_status;
	}
	HAL_FLASH_Unlock();
	HAL_FLASH_ClearError();
	flash_status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, ee.sectors[0].base_addr, VALID_SECTOR);
	HAL_FLASH_Lock();
	if (flash_status != 0)
	{
		return flash_status;
	}

	return FLASH_COMPLETE;
}

/**
 * @brief  Find valid Sector for write or read operation
 * @param  Operation: operation to achieve on the valid sector.
 *   This parameter can be one of the following values:
 *     @arg READ_FROM_VALID_SECTOR: read operation from valid sector
 *     @arg WRITE_IN_VALID_SECTOR: write operation from valid sector
 * @retval returns pointer to the valid sector or NULL if no valid
 *  sector was found
 */
static uint16_t EE_FindValidSector(uint8_t Operation)
{

	/* Get Sector0 actual status */
	ee.sectors[0].status = (*(__IO uint16_t*) ee.sectors[0].base_addr);

	/* Get Sector1 actual status */
	ee.sectors[1].status = (*(__IO uint16_t*) ee.sectors[1].base_addr);

	/* Write or read operation */
	switch (Operation)
	{
		case WRITE_IN_VALID_SECTOR: /* ---- Write operation ---- */
			if (ee.sectors[1].status == VALID_SECTOR)
			{
				/* Sector0 receiving data */
				if (ee.sectors[0].status == RECEIVE_DATA)
				{
					return SECTOR0; /* Sector0 valid */
				}
				else
				{
					return SECTOR1; /* Sector1 valid */
				}
			}
			else if (ee.sectors[0].status == VALID_SECTOR)
			{
				/* Sector1 receiving data */
				if (ee.sectors[1].status == RECEIVE_DATA)
				{
					return SECTOR0; /* Sector0 valid */
				}
				else
				{
					return SECTOR0; /* Sector0 valid */
				}
			}
			else
			{
				return NO_VALID_SECTOR; /* No valid Sector */
			}

		case READ_FROM_VALID_SECTOR: /* ---- Read operation ---- */
			if (ee.sectors[0].status == VALID_SECTOR)
			{
				return SECTOR0; /* Sector0 valid */
			}
			else if (ee.sectors[1].status == VALID_SECTOR)
			{
				return SECTOR1; /* Sector0 valid */
			}
			else
			{
				return NO_VALID_SECTOR; /* No valid Sector */
			}

		default:
			return SECTOR0; /* Sector0 valid */
	}
}

/**
 * @brief  Verify if active sector is full and Writes variable in EEPROM.
 * @param  VirtAddress: 16 bit virtual address of the variable
 * @param  Data: 16 bit data to be written as variable value
 * @retval Success or error status:
 *           - FLASH_COMPLETE: on success
 *           - SECTOR_FULL: if valid sector is full
 *           - NO_VALID_SECTOR: if no valid sector was found
 *           - Flash error code: on write Flash error
 */
static uint16_t EE_VerifySectorFullWriteVariable(uint16_t VirtAddress, uint16_t Data)
{
	FLASH_Status FlashStatus = FLASH_COMPLETE;
	uint16_t ValidSector = 0;
	uint32_t CurrentAddress = 0;

	/* Get valid Sector for write operation */
	ValidSector = EE_FindValidSector(WRITE_IN_VALID_SECTOR);

	/* Check if there is no valid sector */
	if (ValidSector == NO_VALID_SECTOR)
	{
		return NO_VALID_SECTOR;
	}

	/* Get the valid Sector start and end Address */

	CurrentAddress = ee.sectors[ValidSector].base_addr;

	/* Check each active sector address starting from begining */
	while (CurrentAddress < ee.sectors[ValidSector].end_addr)
	{
		/* Verify if CurrentAddress and CurrentAddress+2 contents are 0xFFFFFFFF */
		if ((*(__IO uint32_t*) CurrentAddress) == 0xFFFFFFFF)
		{
			/* Set variable data */
			HAL_FLASH_Unlock();
			HAL_FLASH_ClearError();

			FlashStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, CurrentAddress, Data);
			HAL_FLASH_Lock();
			/* If program operation was failed, a Flash error code is returned */
			if (FlashStatus != FLASH_COMPLETE)
			{
				return FlashStatus;
			}
			/* Set variable virtual address */
			HAL_FLASH_Unlock();
			HAL_FLASH_ClearError();
			FlashStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, CurrentAddress + 2, VirtAddress);
			HAL_FLASH_Lock();
			/* Return program operation status */
			return FlashStatus;
		}
		else
		{
			/* Next address location */
			CurrentAddress = CurrentAddress + 4;
		}
	}

	/* Return SECTOR_FULL in case the valid sector is full */
	return SECTOR_FULL;
}

/**
 * @brief  Transfers last updated variables data from the full Sector to
 *   an empty one.
 * @param  VirtAddress: 16 bit virtual address of the variable
 * @param  Data: 16 bit data to be written as variable value
 * @retval Success or error status:
 *           - FLASH_COMPLETE: on success
 *           - SECTOR_FULL: if valid sector is full
 *           - NO_VALID_SECTOR: if no valid sector was found
 *           - Flash error code: on write Flash error
 */
static uint16_t EE_SectorTransfer(uint16_t VirtAddress, uint16_t Data)
{
	FLASH_Status FlashStatus = FLASH_COMPLETE;
	uint32_t NewSector = 0x080103FF, OldSector = 0x08010000;
	uint16_t ValidSector = 0;
	uint32_t VarIdx = 0;
	uint16_t EepromStatus = 0, ReadStatus = 0;
	uint32_t FlashError;
	FLASH_EraseInitTypeDef sector_erase;
	sector_erase.TypeErase = FLASH_TYPEERASE_SECTORS;
	sector_erase.NbSectors = 1;
	sector_erase.VoltageRange = FLASH_VOLTAGE_RANGE_3;

	/* Get active Sector for read operation */
	ValidSector = EE_FindValidSector(READ_FROM_VALID_SECTOR);

	if (ValidSector == SECTOR1) /* Sector1 valid */
	{
		/* New sector address where variable will be moved to */
		NewSector = SECTOR0;

		/* Old sector address where variable will be taken from */
		OldSector = SECTOR1;
	}
	else if (ValidSector == SECTOR0) /* Sector0 valid */
	{
		/* New sector address where variable will be moved to */
		NewSector = SECTOR1;

		/* Old sector address where variable will be taken from */
		OldSector = SECTOR0;
	}
	else
	{
		return NO_VALID_SECTOR; /* No valid Sector */
	}

	/* Set the new Sector status to RECEIVE_DATA status */
	HAL_FLASH_Unlock();
	HAL_FLASH_ClearError();
	FlashStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, ee.sectors[NewSector].base_addr, (uint16_t) RECEIVE_DATA);
	HAL_FLASH_Lock();
	/* If program operation was failed, a Flash error code is returned */
	if (FlashStatus != FLASH_COMPLETE)
	{
		return FlashStatus;
	}

	/* Write the variable passed as parameter in the new active sector */
	EepromStatus = EE_VerifySectorFullWriteVariable(VirtAddress, Data);
	/* If program operation was failed, a Flash error code is returned */
	if (EepromStatus != FLASH_COMPLETE)
	{
		return EepromStatus;
	}

	/* Transfer process: transfer variables from old to the new active sector */
	for (VarIdx = 0; VarIdx < NumbOfVar; VarIdx++)
	{
		if (VirtAddVarTab[VarIdx] != VirtAddress) /* Check each variable except the one passed as parameter */
		{
			/* Read the other last variable updates */
			ReadStatus = EE_ReadVariable(VirtAddVarTab[VarIdx], &DataVar);
			/* In case variable corresponding to the virtual address was found */
			if (ReadStatus != 0x1)
			{
				/* Transfer the variable to the new active sector */
				EepromStatus = EE_VerifySectorFullWriteVariable(VirtAddVarTab[VarIdx], DataVar);
				/* If program operation was failed, a Flash error code is returned */
				if (EepromStatus != FLASH_COMPLETE)
				{
					return EepromStatus;
				}
			}
		}
	}

	/* Erase the old Sector: Set old Sector status to SECTOR_ERASED status */
	sector_erase.Sector = ee.sectors[OldSector].nbr;
	HAL_FLASH_Unlock();
	HAL_FLASH_ClearError();
	FlashStatus = HAL_FLASHEx_Erase(&sector_erase, &FlashError);
	HAL_FLASH_Lock();
	/* If erase operation was failed, a Flash error code is returned */
	if (FlashStatus != FLASH_COMPLETE)
	{
		return FlashStatus;
	}

	/* Set new Sector status to VALID_SECTOR status */
	HAL_FLASH_Unlock();
	HAL_FLASH_ClearError();
	FlashStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, ee.sectors[NewSector].base_addr, (uint16_t) VALID_SECTOR);
	HAL_FLASH_Lock();

	/* If program operation was failed, a Flash error code is returned */
	if (FlashStatus != FLASH_COMPLETE)
	{
		return FlashStatus;
	}

	/* Get Sector0 status */
	EE_SectorReadStatus(SECTOR0);
	/* Get Sector1 status */
	EE_SectorReadStatus(SECTOR1);

	/* Return last operation flash status */
	return FlashStatus;
}

/**
 * @}
 */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
