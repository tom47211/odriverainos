/*
 * innok_main.h
 *
 *  Created on: 14.10.2019
 *      Author: thomas
 */

#ifndef INNOK_INC_INNOK_MAIN_H_
#define INNOK_INC_INNOK_MAIN_H_


#ifdef __cplusplus

extern "C"
{
#endif

void innok_main();


#ifdef __cplusplus
}

#endif

#endif /* INNOK_INC_INNOK_MAIN_H_ */
