#include "pump.hpp"
#include "cmsis_os.h"
#include "innok_main.h"
#include "eeprom_emulation.h"
#include "eeprom_emulation_variables.h"
#include "innok_can_interface.hpp"
#include "CANopenSDO.hpp"
#include "CANopenDictionary.hpp"
#include "can.h"
#include "main.h"

#define DICTIONARY_SIZE 100


extern CANopenDictionary dictionary;

CAN_Interface can_interface;
SDO_Server sdo_server(&hcan1, &dictionary);

extern Pump::Config_t pump_configs[AXIS_COUNT];

extern Axis *axes[AXIS_COUNT];

Pump *pumps[AXIS_COUNT];

bool requestReset = false;

void start_main_thread();


void innok_main()
{
	HAL_FLASH_Unlock();
	//EE_Init();

	uint16_t boot_flag = 0;

	EE_ReadVariable(EE_ADDR_BOOT_PW,&boot_flag);

	if(boot_flag != 0xBB)
	{
		EE_WriteVariable(EE_ADDR_BOOT_PW, 0xBB);
	}

	for(int i = 0; i<AXIS_COUNT; i++)
	{
		pumps[i] = new Pump(axes[i],&pump_configs[i]);
	}

	can_interface.start_threads();
	sdo_server.start_thread();
	start_main_thread();
}


void mainThread()
{




	for(;;)
	{
		if(requestReset)
		{
			for(int i = 0; i<AXIS_COUNT; i++)
			{
				pumps[i]->stop();
			}
			//To make sure motors get stopped
			osDelay(10);
			NVIC_SystemReset();
		}

		for(int i = 0; i<AXIS_COUNT; i++)
		{
			pumps[i]->loop();
		}

		osDelay(10);

	}
}

void start_main_thread()
{
	osThreadDef(mainThreadDef,(os_pthread) mainThread, osPriorityBelowNormal,0,400);
	osThreadCreate(osThread(mainThreadDef),NULL);
}
