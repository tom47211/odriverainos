/*
 * eeprom_variables.c
 *
 *  Created on: 24.09.2018
 *      Author: thomas
 */
#include "eeprom_emulation_variables.h"


uint16_t VirtAddVarTab[NumbOfVar]=
{
		EE_ADDR_NODE_ID,
		EE_ADDR_BOOT_PW,
		EE_ADDR_CRC_LOW,
		EE_ADDR_CRC_HIGH
};
