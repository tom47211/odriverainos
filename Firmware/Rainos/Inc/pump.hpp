/*
 * pumpe.hpp
 *
 *  Created on: 16.03.2020
 *      Author: thomas
 */

#ifndef RAINOS_PUMP_HPP_
#define RAINOS_PUMP_HPP_

#include "odrive_main.h"


class Pump {

public:

	enum
	{
		Idle,
		Running,
		DryRunProtection,
		Error,
	}typedef State;

	struct
	{
		float velMin = 100.0f;
		float velMax = 1000.0f;
		uint32_t timeout = 100;
		uint32_t maxIqTrips = 100;
		float iqTripLevel = 1.0f;

	}typedef Config_t;

	Pump(Axis *_axis, Config_t *_config) :
			p_axis(_axis),
			config(_config)
	{
	}
	;

	State state = Idle;
	Axis *p_axis;

	Config_t *config;

	void cmdReceived(uint8_t, uint8_t);

	void resetDryRunProtection();
	void resetError();
	void stop();

	void loop();

	float targetVel=0.0f;

	uint32_t lastCommandTimestamp = 0;

	uint32_t iqTripCounter = 0;

	bool dryRunResetFlag = false;
	bool errorResetFlag = false;
	bool active = false;

};

#endif /* RAINOS_PUMP_HPP_ */
